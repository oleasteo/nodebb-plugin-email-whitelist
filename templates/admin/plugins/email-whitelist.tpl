<h1><i class="fa fa-fw {faIcon}"></i> Email whitelist</h1>

<form role="form" class="{nbbId}-settings">
  <fieldset>
    Specify your domain filter patterns:
    <textarea class="form-control" rows="6" id="pattern" name="pattern" placeholder="**"></textarea>
    <button class="btn btn-block btn-lg btn-primary" id="save" type="button">
      <i class="fa fa-fw fa-floppy-o"></i> Save
    </button>
  </fieldset>
</form>

<h2>Syntax cheat sheet</h2>

<ul>
  <li>Whitespace separated patterns (e.g. newline).</li>
  <li><code>*</code> - wildcard (no sub-domains).</li>
  <li><code>**</code> - deep wildcard (matching sub-domains).</li>
  <li><code>!</code> - negates pattern (needs to be at the beginning).</li>
  <li>The last matching pattern decides; allow email address if positive, disallow if negative pattern.</li>
</ul>

<h2>Examples</h2>

<small>Comments (shown with <code>//</code>) are not allowed within the actual syntax.</small>

<p>
  Simple whitelist:
</p>

<pre><code>
email.example.com  // Allow email.example.com
email.*.example.com  // Allow email.XYZ.example.com
**.email.abc.example.com  // Allow email.abc.example.com and all (deep) sub-domains
!email.abc.example.com  // Deny email.abc.example.com (sub-domains are still allowed)
</code></pre>

<p>
  Blacklists using negative patterns:
</p>

<pre><code>
**  // Allow everything
!**.example.com  // Deny example.com and all (deep) sub-domains
email.example.com  // Allow email.example.com
</code></pre>

<h2>Feature overview</h2>

<p>
  Even complex filters depending on email domains are possible using the cheat sheet above.<br/>
  See <a href="https://www.npmjs.com/package/ns-matcher">ns-matcher</a> for details and all features; keep the
  domain-targeting options (e.g. <code>.</code> as sub-domain separator) in mind.
</p>

<script type="text/javascript">
  require(["settings"], function (Settings) {
    var nbbId = "{nbbId}", wrapper = $("." + nbbId + "-settings");

    Settings.load(nbbId, wrapper);

    wrapper.find("#save").on("click", function (e) {
      e.preventDefault();
      Settings.save(nbbId, wrapper, function () {
        app.alert({
          type: "success",
          alert_id: nbbId,
          title: "Reload Required",
          message: "Please reload your NodeBB to have your changes take effect",
          clickfn: function () { socket.emit("admin.reload"); }
        });
      });
    });
  });
</script>