# NodeBB plugin - Email whitelist

[![License](https://img.shields.io/npm/l/nodebb-plugin-email-whitelist.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/nodebb-plugin-email-whitelist.svg)](https://www.npmjs.com/package/nodebb-plugin-email-whitelist)
[![Downloads](https://img.shields.io/npm/dm/nodebb-plugin-email-whitelist.svg)](https://www.npmjs.com/package/nodebb-plugin-email-whitelist)

Allows you to specify patterns for an email address domain whitelist during registration and email modification.

> The pattern system actually allows blacklists as well :D

## Syntax cheat sheet

* Whitespace separated patterns (e.g. newline).
* `*` - wildcard (no sub-domains).
* `**` - deep wildcard (matching sub-domains).
* `!` - negates pattern (needs to be at the beginning).
* The last matching pattern decides; allow email address if positive, disallow if negative pattern.

## Examples

Comments (shown with `//`) are not allowed within the actual syntax.

Simple whitelist:

```
email.example.com  // Allow email.example.com
email.*.example.com  // Allow email.XYZ.example.com
**.email.abc.example.com  // Allow email.abc.example.com and all (deep) sub-domains
!email.abc.example.com  // Deny email.abc.example.com (sub-domains are still allowed)
```

Blacklists using negative patterns:

```
**  // Allow everything
!**.example.com  // Deny example.com and all (deep) sub-domains
email.example.com  // Allow email.example.com
```

## Feature overview

Even complex filters depending on email domains are possible using the cheat sheet above.

See [ns-matcher](https://www.npmjs.com/package/ns-matcher) for details and all features; keep the
domain-targeting options (e.g. <code>.</code> as sub-domain separator) in mind.
